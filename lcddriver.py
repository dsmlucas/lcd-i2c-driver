from enum import Enum
from time import *
import math
import smbus

# LCD Address
# ADDRESS = 0x3F
ADDRESS = 0x27
MAX_WIDTH = 20

# I2C bus
BUS = 1

# commands
LCD_CLEARDISPLAY = 0x01
LCD_RETURNHOME = 0x02
LCD_ENTRYMODESET = 0x04
LCD_DISPLAYCONTROL = 0x08
LCD_CURSORSHIFT = 0x10
LCD_FUNCTIONSET = 0x20
LCD_SETCGRAMADDR = 0x40
LCD_SETDDRAMADDR = 0x80

# flags for display entry mode
LCD_ENTRYRIGHT = 0x00
LCD_ENTRYLEFT = 0x02
LCD_ENTRYSHIFTINCREMENT = 0x01
LCD_ENTRYSHIFTDECREMENT = 0x00

# flags for display on/off control
LCD_DISPLAYON = 0x04
LCD_DISPLAYOFF = 0x00
LCD_CURSORON = 0x02
LCD_CURSOROFF = 0x00
LCD_BLINKON = 0x01
LCD_BLINKOFF = 0x00

# flags for display/cursor shift
LCD_DISPLAYMOVE = 0x08
LCD_CURSORMOVE = 0x00
LCD_MOVERIGHT = 0x04
LCD_MOVELEFT = 0x00

# flags for function set
LCD_8BITMODE = 0x10
LCD_4BITMODE = 0x00
LCD_2LINE = 0x08
LCD_1LINE = 0x00
LCD_5x10DOTS = 0x04
LCD_5x8DOTS = 0x00

# flags for backlight control
LCD_BACKLIGHT = 0x08
LCD_NOBACKLIGHT = 0x00

En = 0b00000100  # Enable bit
Rw = 0b00000010  # Read/Write bit
Rs = 0b00000001  # Register select bit


class TextPosition(Enum):
    LEFT = 'left'
    CENTER = 'center'
    RIGHT = 'right'


class LCD:
    """
    Class to control the 20x2 I2C LCD display from sainsmart from the Raspberry Pi
    """

    def __init__(self):
        """Setup the display, turn on backlight and text display + ...?"""
        self.device = I2CDevice(ADDRESS, BUS)

        self.write(0x03)
        self.write(0x03)
        self.write(0x03)
        self.write(0x02)

        self.write(LCD_FUNCTIONSET | LCD_2LINE | LCD_5x8DOTS | LCD_4BITMODE)
        self.write(LCD_DISPLAYCONTROL | LCD_DISPLAYON)
        self.write(LCD_CLEARDISPLAY)
        self.write(LCD_ENTRYMODESET | LCD_ENTRYLEFT)
        sleep(0.2)

    def strobe(self, data):
        """Clocks EN to latch command"""
        self.device.write_cmd(data | En | LCD_BACKLIGHT)
        sleep(0.0005)
        self.device.write_cmd(((data & ~En) | LCD_BACKLIGHT))
        sleep(0.001)

    def write_four_bits(self, data):
        self.device.write_cmd(data | LCD_BACKLIGHT)
        self.strobe(data)

    def write(self, cmd, mode=0):
        """Write a command to lcd"""
        self.write_four_bits(mode | (cmd & 0xF0))
        self.write_four_bits(mode | ((cmd << 4) & 0xF0))

    def display_string(self, text: str, line: int, position=TextPosition.LEFT):
        if line == 1:
            self.write(0x80)
        if line == 2:
            self.write(0xC0)
        if line == 3:
            self.write(0x94)
        if line == 4:
            self.write(0xD4)

        limited_text = text[0:MAX_WIDTH]
        remain = MAX_WIDTH - len(limited_text)

        if position == TextPosition.CENTER:
            spaces = math.floor(remain / 2)
            limited_text = (spaces * ' ') + limited_text
        elif position == TextPosition.RIGHT:
            limited_text = (remain * ' ') + limited_text

        for char in limited_text:
            self.write(ord(char), Rs)

    def clear(self):
        """Clear lcd and set to home"""
        self.write(LCD_CLEARDISPLAY)
        self.write(LCD_RETURNHOME)

    def backlight_off(self):
        """Turn off backlight, anything that calls write turns it on again"""
        self.clear()
        self.device.write_cmd(LCD_NOBACKLIGHT)

    def display_off(self):
        """Turn off the text display"""
        self.write(LCD_DISPLAYCONTROL | LCD_DISPLAYOFF)

    def display_on(self):
        """Turn on the text display"""
        self.write(LCD_DISPLAYCONTROL | LCD_DISPLAYON)

    def keyboard_stop(self):
        """Notify about keyboard stop and turn off the text display"""
        exit_msg = 'Stopped by keyboard'
        exit_delay = 3

        self.clear()
        self.display_string(exit_msg, 1)
        self.display_string('Backlight off in ', 3)

        for n in range(exit_delay, 0, -1):
            self.display_string(str(n) + ' second(s)', 4)
            sleep(1)

        self.clear()
        self.backlight_off()


class I2CDevice:
    def __init__(self, addr, port=1):
        self.addr = addr
        self.bus = smbus.SMBus(port)

    def write_cmd(self, cmd):
        """Write a single command"""
        self.bus.write_byte(self.addr, cmd)
        sleep(0.0001)

    def write_cmd_arg(self, cmd, data):
        """Write a command and argument"""
        self.bus.write_byte_data(self.addr, cmd, data)
        sleep(0.0001)

    def write_block_data(self, cmd, data):
        """Write a block of data"""
        self.bus.write_block_data(self.addr, cmd, data)
        sleep(0.0001)

    def read(self):
        """Read a single byte"""
        return self.bus.read_byte(self.addr)

    def read_data(self, cmd):
        """Read"""
        return self.bus.read_byte_data(self.addr, cmd)

    def read_block_data(self, cmd):
        """Read a block of data"""
        return self.bus.read_block_data(self.addr, cmd)
