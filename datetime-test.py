from lcddriver import LCD, TextPosition
from time import sleep
from datetime import datetime

lcd = LCD()

lcd.display_string('Hello, Lucas!', 1, TextPosition.CENTER)

while True:
    dateString = datetime.now().strftime('%a, %b %d, %Y')
    timeString = datetime.now().strftime('%H:%M:%S')

    lcd.display_string(dateString, 3)
    lcd.display_string(timeString, 4)
    sleep(1)
