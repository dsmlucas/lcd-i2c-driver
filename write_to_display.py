from time import sleep
from lcddriver import LCD
import sys

__MAX_ARGS = 4


try:
    args_size = len(sys.argv) - 1

    if args_size > __MAX_ARGS:
        error_msg_fmt = 'Too many args! Maximum args are: {}'
        raise Exception(error_msg_fmt.format(__MAX_ARGS))

    my_lcd = LCD()

    if args_size:
        for i in range(args_size):
            line = str(sys.argv[i + 1])
            print('Printing on line {}: {}'.format(i + 1, line))
            my_lcd.display_string(line, i + 1)

except KeyboardInterrupt:
    exit_msg = 'Stopped by keyboard'
    exit_delay = 3

    my_lcd.clear()
    my_lcd.display_string(exit_msg, 1)
    my_lcd.display_string('Backlight off in ', 2)

    for n in range(exit_delay, 0, -1):
        my_lcd.display_string(str(n) + ' second(s)', 3)
        sleep(1)

    my_lcd.clear()
    my_lcd.backlight_off()
